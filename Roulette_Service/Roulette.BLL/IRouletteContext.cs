﻿using System;
using System.Collections.Generic;
using System.Text;
using Roulette.Library.DAO;

namespace Roulette.BLL
{
    public interface IRouletteContext
    {
        public DRoulette CreateRoulette();        

        public bool InsertGameRecord(DGameRecord dGameRecord);

    }
}
