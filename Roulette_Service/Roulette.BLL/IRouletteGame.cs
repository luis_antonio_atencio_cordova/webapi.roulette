﻿using System;
using System.Collections.Generic;
using System.Text;
using Roulette.Library.DTO;

namespace Roulette.BLL
{
    public interface IRouletteGame
    {
        //bool openRoulette(int id);
        //List<BetRecord> closeRoulette(int id);

        void RouletteSpinResults(ref List<BetRecord> bets);
                
        void spinResult(ref BetRecord bet, int result);
    }
}
