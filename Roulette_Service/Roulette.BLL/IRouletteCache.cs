﻿using System;
using System.Collections.Generic;
using System.Text;
using Roulette.Library.DTO;

namespace Roulette.BLL
{
    interface IRouletteCache
    {
        RouletteD createRoulette();
        

        RouletteD openRoulette(int id);

        List<BetRecord> closeRoulette(int id);


        bool createBetRecord(BetRecord bet, User user);

        List<BetRecord> getAllBetRecords();

    }
}
