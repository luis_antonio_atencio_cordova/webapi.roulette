﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Roulette.Library.DTO;
using Roulette.API.Extensions;
using Microsoft.Extensions.Caching.Distributed;
using Roulette.API.Game;
using Roulette.BLL;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Roulette.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RouletteGameController : ControllerBase
    {
        private IDistributedCache _rouleteCache { get; }
        private IRouletteContext _rouletteContext { get; }

        public RouletteGameController(IDistributedCache rouleteCache, IRouletteContext rouletteContext)
        {
            _rouleteCache = rouleteCache;
            _rouletteContext = rouletteContext;
        }

        // GET: api/<RouletteGameController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<RouletteD>> CreateRoulette()
        {
            var roulette = _rouletteContext.CreateRoulette();

            if(roulette == null)
            {
                return NotFound("La ruleta no pudo ser creada");
            }

            string recordKey = "Roulette_" + roulette.id;
            var rouletteD = new RouletteD { id = roulette.id, open = false,timeStamp = roulette.timeStamp };
            await _rouleteCache.SetRecordAsync(recordKey, rouletteD);

            return Ok(rouletteD);
        }


        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<RouletteD>> OpenRoulette(int id)
        {
            string recordKey = "Roulette_" + id;

            var rouletteD = await _rouleteCache.GetRecordAsync<RouletteD>(recordKey);

            if (rouletteD == null)
            {
                return NotFound("La Ruleta No Existe");
            }

            rouletteD.open = true;

            await _rouleteCache.UpdateRecordAsync(recordKey, rouletteD);

            return Ok(rouletteD);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult<List<BetRecord>>> NewBetRecord(BetRecord bet)
        {
            bet.timeStamp = DateTime.Now;            

            string recordKey = string.Format("BetRecords_{0}", bet.rouletteId);
            string recordKeyRoulette = string.Format("Roulette_{0}", bet.rouletteId);

            var rouletteD = await _rouleteCache.GetRecordAsync<RouletteD>(recordKeyRoulette);

            if(rouletteD == null)
            {
                return NotFound("La Ruleta No Existe");
            }

            if (!rouletteD.open)
            {
                return NotFound("La Ruleta No Esta Abierta");
            }


            List<BetRecord> betRecords = await _rouleteCache.GetRecordAsync<List<BetRecord>>(recordKey);

            if (betRecords == null)
            {
                betRecords = new List<BetRecord>();                
            }

            betRecords.Add(bet);

            await _rouleteCache.RemoveRecordAsync(recordKey);
            await _rouleteCache.SetRecordAsync(recordKey, betRecords);
            
            return Ok(betRecords);
        }


        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<List<BetRecord>>> CloseRoulette(int id)
        {
            DateTime close = DateTime.Now;
            string recordKeyRoulette = string.Format("Roulette_{0}", id);
            string recordKey = string.Format("BetRecords_{0}", id);

            var rouletteD = await _rouleteCache.GetRecordAsync<RouletteD>(recordKeyRoulette);

            if (rouletteD == null)
            {
                return NotFound("No se encontro la ruleta");
            }

            var betRecords = await _rouleteCache.GetRecordAsync<List<BetRecord>>(recordKey);            
            
            if (betRecords == null)
            {
                return Ok("Ruleta Cerrada, Sin Apuestas");
            }

            foreach(var bet in betRecords)
            {
                _rouletteContext.InsertGameRecord(new Library.DAO.DGameRecord
                {
                    userId = bet.userId,
                    rouletteid = bet.rouletteId,
                    bet = bet.bet,
                    paid = bet.paid,
                    data = bet.type.ToString() + "_" + bet.value.ToString(),
                    timeStampBet = bet.timeStamp,
                    timeStampClose = close
                });
            }

            RouletteGame.RouletteSpinResults(ref betRecords);

            await _rouleteCache.RemoveRecordAsync(recordKey);
            await _rouleteCache.RemoveRecordAsync(recordKeyRoulette);

            return Ok(betRecords);
        }

    }
}

