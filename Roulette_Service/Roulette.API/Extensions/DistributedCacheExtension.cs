﻿using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Roulette.API.Extensions
{
    public static class DistributedCacheExtension
    {
        public static async Task SetRecordAsync<T>( this IDistributedCache cache,                                                        
                                                    string recordId,
                                                    T data,
                                                    TimeSpan? absoluteExpireTime = null,
                                                    TimeSpan? unUsedExpireTime = null)
        {
            var options = new DistributedCacheEntryOptions();
            options.AbsoluteExpirationRelativeToNow = absoluteExpireTime ?? TimeSpan.FromSeconds(500); // wait max expiration time
            options.SlidingExpiration = unUsedExpireTime; // if its not used, its not gona be explired from this timespan // if its null no expiration

            var jsonData = JsonSerializer.Serialize(data);// convert data to json

            await cache.SetStringAsync(recordId, jsonData, options); // insert data to redis
        }

        public static async Task<T> GetRecordAsync<T>(  this IDistributedCache cache,string recordId) {
            var jsonData = await cache.GetStringAsync(recordId);

            if (jsonData == null)
            {
                return default(T);// return the tipe of value null depending of the data
            }

            return JsonSerializer.Deserialize<T>(jsonData);
        }

        public static async Task UpdateRecordAsync<T>(this IDistributedCache cache, string recordId, T newData)
        {
            await cache.RemoveAsync(recordId);

            var options = new DistributedCacheEntryOptions();
            options.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(120);
            options.SlidingExpiration = null;

            var jsonData = JsonSerializer.Serialize(newData);

            await cache.SetStringAsync(recordId, jsonData, options);

        }

        public static async Task RemoveRecordAsync(this IDistributedCache cache, string recordId) 
        {
            await cache.RemoveAsync(recordId);
        }
    }
}
