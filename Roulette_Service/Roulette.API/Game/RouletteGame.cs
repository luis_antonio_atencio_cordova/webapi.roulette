﻿using Roulette.BLL;
using Roulette.Library.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Roulette.API.Game
{
    public class RouletteGame
    {
        public static void RouletteSpinResults(ref List<BetRecord> bets)
        {
            Random R = new Random();
            int winNumber = R.Next(0, 36);

            for (int i = 0; i < bets.Count; i ++){
                BetRecord betRecord = bets[i];
                spinResult(ref betRecord, winNumber);
            }
            
        }

        public static void spinResult(ref BetRecord bet, int result)
        {
            if(bet.type == 1)//apuesta a numero
            {
                if(bet.value == result)
                {
                    bet.paid = bet.bet * 5;
                }
                else
                {
                    bet.paid = 0;
                }
            }
            else if (bet.type == 2)//apuesta a color
            {
                bool colorBet = bet.value % 2 == 0;
                bool colorRes = result % 2 == 0;

                if (colorBet == colorRes)
                {
                    bet.paid = bet.bet * 1.8;
                }
                else
                {
                    bet.paid = 0;
                }
            }
            else
            {
                bet.paid = 0;
            }
        }

    }
}
