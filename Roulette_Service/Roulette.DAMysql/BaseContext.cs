﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.DAMysql
{
    public class BaseContext
    {
        public string ConnectionString = "server=127.0.0.1;database=testroulette;UID=root;password=root";

        protected MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }
    }
}
