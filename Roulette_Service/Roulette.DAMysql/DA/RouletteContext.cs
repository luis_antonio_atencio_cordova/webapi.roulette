﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using Roulette.BLL;
using Roulette.Library.DAO;

namespace Roulette.DAMysql.DA
{
    public class RouletteContext: BaseContext, IRouletteContext
    {
        public DRoulette CreateRoulette()
        {
            DRoulette result = null;
            try
            {
                using (MySqlConnection conn = GetConnection())
                {
                    conn.Open();

                    string mysql_command = "insert into testroulette.roulette () values (); Select * from testroulette.roulette where id = LAST_INSERT_ID();";
                    MySqlCommand cmd = new MySqlCommand(mysql_command, conn);                    
                    var reader = cmd.ExecuteReader();                    
                    while (reader.Read())
                    {
                        result = new DRoulette 
                        {
                            id = Convert.ToInt32(reader["id"]),
                            timeStamp = Convert.ToDateTime(reader["time_stamp"]),
                        };                        
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return result;
        }

        public bool InsertGameRecord(DGameRecord dGameRecord)
        {            
            try
            {
                using (MySqlConnection conn = GetConnection())
                {
                    conn.Open();

                    string mysql_command = "insert into testroulette.game_record (rouletteid,userid,bet,paid,data,time_stamp_bet,time_stamp_close) values (@rouletteid,@userid,@bet,@paid,@data,@time_stamp_bet,@time_stamp_close);";
                    MySqlCommand cmd = new MySqlCommand(mysql_command, conn);
                    cmd.Parameters.AddWithValue("@rouletteid", dGameRecord.rouletteid);
                    cmd.Parameters.AddWithValue("@userid", dGameRecord.userId);
                    cmd.Parameters.AddWithValue("@bet", dGameRecord.bet);
                    cmd.Parameters.AddWithValue("@paid", dGameRecord.paid);
                    cmd.Parameters.AddWithValue("@data", dGameRecord.data);
                    cmd.Parameters.AddWithValue("@time_stamp_bet", dGameRecord.timeStampBet);
                    cmd.Parameters.AddWithValue("@time_stamp_close", dGameRecord.timeStampClose);

                    cmd.ExecuteNonQuery();
                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
                
            }
            return true;

        }
    }
}
