﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette.Library.DTO
{
    public class User
    {
        public int id { get; set; }
        public string username { get; set; }
        public double balance { get; set; }
    }
}
