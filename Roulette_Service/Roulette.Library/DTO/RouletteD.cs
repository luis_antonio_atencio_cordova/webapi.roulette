﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette.Library.DTO
{
    public class RouletteD
    {
        public int id { get; set; }
        public bool open { get; set; }
        public DateTime timeStamp { get; set; }
    }
}
