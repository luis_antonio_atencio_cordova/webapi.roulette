﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette.Library.DTO
{
    public class BetRecord
    {
        public int rouletteId { get; set; }
        public int userId { get; set; }
        public double bet { get; set; }
        public int type { get; set; }
        public int value { get; set; }
        public double paid { get; set; }
        public DateTime timeStamp { get; set; }
    }
}
