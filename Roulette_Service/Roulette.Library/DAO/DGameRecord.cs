﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Roulette.Library.DAO
{
    public class DGameRecord
    {
        public int rouletteid { get; set; }
        public int userId { get; set; }
        public double bet { get; set; }
        public double paid { get; set; }
        public string data { get; set; }
        public DateTime timeStampBet { get; set; }
        public DateTime timeStampClose { get; set; }

    }
}
